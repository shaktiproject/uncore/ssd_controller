SSD-Controller Configuration/Testing
====================================

This Readme provides the procedure for building configuring the SSD Controller on a Xilinx Dev board(should
have PCIe hard IP, we will support Altera soon) and installing the NVMe CLI driver on the host machine 
to test the controller).
The controller support some basic MVMe commans, it will shortly support most of the
NVMe command set and also the Lightstor(TM) command set, an enhacement to NVMe based on the
OpenChannelSSD work from ITU, Copenhagen. This command set will also support computing
using teh programmable CPUs in the controllers(tyep 4 SSDs in our parlance)

Currently both ONFI and Flash are emulated. We will integrate the ONFI controller (v 4.0) once
verification is complete. For using actual flash, users will have 2 options. A FMC add card card with Micron 3D 
NAND is getting tested. Once that is available, any FPGA dev board with PCIe hard IP and FMC connectors can
be configured as an SSD system. The card's schematics and gerber will be released into open source.

The second option is a dedicated SSD card designed by IIT-Madras and HCL that is
getting ready for fabrication. It will be similar to  commercial SSD cards and has connectors for
NAND modules. It  will support both PCIe (8x) and Serial RapidIO ( 4x 10/25G via QSFP). We intend to make the NAND 
modules and connectors an open standard so that 3rd party vendors
can create SSD modules for the card. Modules can be any kind of NAND or NV memory as long as the electrical
interface from the FPGA to the module can be programmed to support the required interface. In practical
terms, it means all NAND devices should be supportable (we are cheking DDR type interfaces and toggle
interfaces later). Presumably various resitive RAM and PCM devices which have DRAM type interfaces
should also work (in theory !). The card is inteneded for commercial and BOM optimized deployment.
This would only require a smaller FPGA or an ASIC to replace the rather large FPGA we are using.



#Downloading the SSD controller repository and building the NVMe CLI tool

   First, Clone the ssd-controller repository

	$ git clone https://bitbucket.org/casl/ssd-controller.git

   Then, build and install the NVM-Express user space tools for linux (NVMe cli) 

	$ https://github.com/linux-nvme/nvme-cli.git

	$ cd nvme-cli

	$ make && make install 

   It can also alternatively be installed  as follows (Ubuntu)

	$ sudo add-apt-repository ppa:sbates 

	$ sudo apt-get update

	$ sudo apt-get install nvme-cli

   Kernel Version above 4.4.2 is recommended.


#Usage

 After cloning and installation of the repositories, install the FPGA dev board
 on an available PCIe slot in your test PC. Follow Xilinx's instructions for this.

 If Xilinx Artix-7 (AC701 Evaluation kit) is used, the bitstream provided in tis repository can be used. 
 For other boards, the bitstream needs to be generated from the provided Verilog files 
 (Xilinx Vivado 2016.1 is recommended for Bitstream Generation). 
 We have tested only with the Xilinx PCIe hard IP but with some tweaking an PCIe IP should be usable. 

  After the bitstream is loaded, run teh list PCI devices command
  
	 $ lspci

   in the host system which will show list of PCI devices. 
   The SSD Controller should be detected, if not, reboot the host system till it is detected. 

  After the Controller is detected, the Kernel module should be loaded as follows,

	$ cd /path/to/linux/kernel/include/drivers/nvme/host
   
	$ sudo make

	$ sudo insmod nvme.ko
   
   The driver message command can be used to see the presence of the modules and observe
   diagnostic messages pertaining to the NVMe driver

	$ dmesg

   Alternatively, a modified Kernel module is provided in the repository with 
   additional display driver messages for better visibility of transactions between 
   the host and the controller which can be built   as follows,
 
	$ cd ssd-controller

	$ cd nvme

	$ ./script.sh
  
  After the Kernel module is loaded, check whether Nvme block device is created in the /dev directory

	$ ls /dev/nvme0

	$ ls /dev/nvme0n1

  From the NVMe cli, the list command can be used to list the available NVMe devices connected to the PC
  
	$ sudo nvme list 

  Now different types of admin and IO commands commands can be run using NVMe-cli. 
  To get the list of available commands and its usage use either of the commands,
   
	$ sudo nvme help

	$ man nvme

#Work in Progress

 The controller is in its verification phase.

 It is being integrated with the NAND flash controller. Presently, an emulated Block RAM model is used.
