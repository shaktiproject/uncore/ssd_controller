/*
 * Generated by Bluespec Compiler, version 2015.09.beta2 (build 34689, 2015-09-07)
 * 
 * On Tue Jul 19 13:21:16 IST 2016
 * 
 */

/* Generation options: */
#ifndef __module_fn_sra_srl_h__
#define __module_fn_sra_srl_h__

#include "bluesim_types.h"
#include "bs_module.h"
#include "bluesim_primitives.h"
#include "bs_vcd.h"
#include "module_fn_shiftright.h"


/* Class declaration for the module_fn_sra_srl module */
class MOD_module_fn_sra_srl : public Module {
 
 /* Clock handles */
 private:
 
 /* Clock gate handles */
 public:
  tUInt8 *clk_gate[0];
 
 /* Instantiation parameters */
 public:
 
 /* Module state */
 public:
  MOD_module_fn_shiftright INST_instance_fn_shiftright_1;
  MOD_module_fn_shiftright INST_instance_fn_shiftright_0;
 
 /* Constructor */
 public:
  MOD_module_fn_sra_srl(tSimStateHdl simHdl, char const *name, Module *parent);
 
 /* Symbol init methods */
 private:
  void init_symbols_0();
 
 /* Reset signal definitions */
 private:
 
 /* Port definitions */
 public:
 
 /* Publicly accessible definitions */
 public:
 
 /* Local definitions */
 private:
  tUWide DEF_fn_shiftright___d18;
  tUWide DEF__0_CONCAT_IF_fn_sra_srl_op_32_AND_fn_sra_srl_rl_ETC___d11;
  tUWide DEF_fn_shiftright___d21;
  tUWide DEF__18446744073709551615_CONCAT_IF_fn_sra_srl_op_3_ETC___d20;
 
 /* Rules */
 public:
 
 /* Methods */
 public:
  tUInt64 METH_fn_sra_srl(tUInt64 ARG_fn_sra_srl__in1,
			  tUInt64 ARG_fn_sra_srl__in2,
			  tUInt32 ARG_fn_sra_srl__immediate,
			  tUInt8 ARG_fn_sra_srl_rl_ra_flag,
			  tUInt8 ARG_fn_sra_srl__imm_flag,
			  tUInt8 ARG_fn_sra_srl_op_32);
  tUInt8 METH_RDY_fn_sra_srl();
 
 /* Reset routines */
 public:
 
 /* Static handles to reset routines */
 public:
 
 /* Pointers to reset fns in parent module for asserting output resets */
 private:
 
 /* Functions for the parent module to register its reset fns */
 public:
 
 /* Functions to set the elaborated clock id */
 public:
 
 /* State dumping routine */
 public:
  void dump_state(unsigned int indent);
 
 /* VCD dumping routines */
 public:
  unsigned int dump_VCD_defs(unsigned int levels);
  void dump_VCD(tVCDDumpType dt, unsigned int levels, MOD_module_fn_sra_srl &backing);
  void vcd_defs(tVCDDumpType dt, MOD_module_fn_sra_srl &backing);
  void vcd_submodules(tVCDDumpType dt, unsigned int levels, MOD_module_fn_sra_srl &backing);
};

#endif /* ifndef __module_fn_sra_srl_h__ */
