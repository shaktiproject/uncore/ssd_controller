#! /usr/bin/env python

""" This script can be used to make the transcript of modelsim a bit more
colorful in the terminal. """

import sys

RED = "\033[31m"
GREEN = "\033[32m"
YELLOW = "\033[33m"
BLUE = "\033[34m"
BOLD = "\033[1m"
UNDER = "\033[4m"
BGRED = "\033[41m"
BGGREEN = "\033[42m"
BGYELLOW = "\033[43m"
BGBLUE = "\033[44m"
BGBLACK = "\033[40m"
BGBLACKHIGH = "\033[100m"
NEGATIVE = "\033[7m"
DEFAULT = "\033[m"

highlight = 'valid completion'

color_dict = {
	'Testbench': YELLOW,
	'main_memory_model:': GREEN,
	'NvmController:': BLUE
}

with open(sys.argv[1], "r") as f:
	last_clk = ''
	bg = BGBLACK
	not_bg = ''
	for line in f:
		line = line.strip()
		parts = line.split()
		identifier = ''
		if len(parts) > 2:
			identifier = parts[2]
		if len(parts) > 1:
			if parts[1] != last_clk:
				bg, not_bg = not_bg, bg
				last_clk = parts[1]
		hi = ''
		if highlight in line:
			hi = RED
		print (bg + color_dict.get(identifier, '') + hi + (line).ljust(132) +
			DEFAULT)
